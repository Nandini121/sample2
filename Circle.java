
public class Circle {
	
	// PROPERTIES
	// --------------------
	private double radius = 5;
	
		
	// CONSTRUCTOR
	// --------------------
	public Circle(double r) {
		this.radius = r+2;
	}
	
	// CUSTOM METHODS
	// --------------------
	public void getArea() {
		double area = Math.PI * this.radius * this.radius;
		System.out.println("Area: " + area);
	}
	public void getCircumference() {
		double c = Math.PI * 2 * this.radius;
		System.out.println("Circumfrence: " + c);
	}
	public void getDiameter() {
		double diameter = Math.PI * this.radius;
		System.out.println("Diameter: "+ diameter);
	}
	
	
	// GETTERS AND SETTERS
	// --------------------
	public double getRadius() {
		return radius;
	}


	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	
	
	
	
	
}
