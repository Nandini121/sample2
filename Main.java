
public class Main {

	public static void main(String[] args) {
		
		
		// R1: Constructor sets radius
		// Expected:  radius = 5
		// Actual:  c.getRadius()
		Circle c = new Circle(5);
		
		double expectedRadius = 5;
		double actualRadius = c.getRadius();
		
		if (expectedRadius == actualRadius) {
			System.out.println("R1: Working!");
		}
		else {
			System.out.println("R1: NOT Working!");
			System.out.println("Expected Result: " + expectedRadius);
			System.out.println("Actual Result: " + actualRadius);
		}
		
		
		
		
		
		
		// PROCESS FOR TESTING CODE
		// ----------------------------
		// 1. Read the requirements!
		// 2. Decide what to test!
				// R1:  Constructor sets radius
				// R2:  Areas calculates properly
				// R3:  Circufum calcuates properly
				// R4:  Diamemter calculates properly
		// 3. Make test cases
		//		// Test case = the thing you are trying to test
				// Working?
				// Expected Result  --> What SHOULD happen
				// Actual Result	--> What ACTUALLY happened
		
				// Test case:
				//		-- Working   (expected == actual)
				//		-- Not work	 (expected != actual)

		
		
	}

}
